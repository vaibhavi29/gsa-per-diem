require 'spreadsheet'

workbook = Spreadsheet.open './FY2012-PerDiemRatesMasterFile.xls'
sqlFile = File.new("gsa-per-diem-rates-#{Time.now.strftime("%Y%m%d-%H%m%S")}.plist", "w")

worksheet = workbook.worksheet(0)
j = 0

sqlFile.puts "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
<plist version=\"1.0\">
<array>"
worksheet.each { |row|

  if (row != nil)
     if (j != 0) 
       sqlFile.puts "   <dict>
    <key>state</key>
    <string>" + row[1].to_s + "</string>
    <key>destination</key>
    <string>" + row[2].to_s + "</string>
    <key>county</key>
    <string>" + row[3].to_s + "</string>
    <key>date_begin</key>
    <string>" + row[4].to_s + "</string>
    <key>date_end</key>
    <string>" + row[5].to_s + "</string>
    <key>rate_lodging</key>
    <real>" + row[6].to_s + "</real>
    <key>rate_mie</key>
    <real>" + row[7].to_s + "</real>
  </dict>"
    end
     j = j + 1
   end
}
sqlFile.puts "</array>
</plist>"

