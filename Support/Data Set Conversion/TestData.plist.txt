<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
	<dict>
		<key>title</key>
		<string>Batman Begins</string>
		<key>coverImage</key>
		<string>batman_begins.png</string>
		<key>featureLength</key>
		<integer>140</integer>
		<key>releaseDate</key>
		<date>2008-07-08T23:32:07Z</date>
	</dict>
	<dict>
		<key>title</key>
		<string>The Dark Knight</string>
		<key>coverImage</key>
		<string>dark_knight.png</string>
		<key>featureLength</key>
		<integer>152</integer>
		<key>releaseDate</key>
		<date>2008-12-10T00:32:07Z</date>
	</dict>
	<dict>
		<key>title</key>
		<string>The Prestige</string>
		<key>coverImage</key>
		<string>prestige.png</string>
		<key>featureLength</key>
		<integer>130</integer>
		<key>releaseDate</key>
		<date>2007-02-21T00:32:07Z</date>
	</dict>
</array>
</plist>
