//
//  DestinationDetailViewController.m
//  GSA Per Diem
//
//  Created by Christopher Graham on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DestinationDetailViewController.h"

@interface DestinationDetailViewController ()

@end

@implementation DestinationDetailViewController

@synthesize stateLabel;
@synthesize countyLabel;
@synthesize destinationLabel;
@synthesize dateBeginLabel;
@synthesize dateEndLabel;
@synthesize rateLodgingLabel;
@synthesize rateMIELabel;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  self.stateLabel.text = @"test";
}

- (void)viewDidUnload
{
    [self setStateLabel:nil];
    [self setCountyLabel:nil];
    [self setDestinationLabel:nil];
    [self setDateBeginLabel:nil];
    [self setDateEndLabel:nil];
    [self setRateLodgingLabel:nil];
    [self setRateMIELabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
