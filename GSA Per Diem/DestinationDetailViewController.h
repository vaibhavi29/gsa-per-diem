//
//  DestinationDetailViewController.h
//  GSA Per Diem
//
//  Created by Christopher Graham on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DestinationDetailViewController : UITableViewController {
  
}

@property (strong, nonatomic) IBOutlet UILabel *stateLabel;
@property (strong, nonatomic) IBOutlet UILabel *countyLabel;
@property (strong, nonatomic) IBOutlet UILabel *destinationLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateBeginLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateEndLabel;
@property (strong, nonatomic) IBOutlet UILabel *rateLodgingLabel;
@property (strong, nonatomic) IBOutlet UILabel *rateMIELabel;
@end
