//
//  RateFetcher.m
//  GSA Per Diem
//
//  Created by Christopher Graham on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RateFetcher.h"

@implementation RateFetcher

@synthesize rateContent = _rateContent;
@synthesize ratePlist = _ratePlist;
//@synthesize rateSections = _rateSections;

- (id)initWithRateName:(NSString *)rateName {
  
  if (self = [super init]) {
    ratePlist = rateName;
    NSString *myFile2 = [[NSBundle mainBundle] pathForResource:rateName ofType:@"plist"];
    rateContent = [[NSArray alloc] initWithContentsOfFile:myFile2];
//    NSLog(@"RateFetcher - rateContent [%@]", rateContent);
    
//    BOOL found;
//    rateSections = [[NSMutableDictionary alloc]init ];
    
//    // Loop through the books and create our keys
//    for (NSDictionary *rate in rateContent) {
//      NSString *c = [[rate objectForKey:@"state"] substringToIndex:1];
//      found = NO;
//      for (NSString *str in [rateSections allKeys]) {
//        if ([str isEqualToString:c]) {
//          found = YES;
//        }
//      }
//      
//      if (!found){
//        [rateSections setValue:[[NSMutableArray alloc] init] forKey:c];
//        NSLog(@"RateFetcher - rateSections [%@]", rateSections);
//      }
//    }
  }
//  NSLog(@"RateFetcher - rateSections [%@]", rateSections);

  return self;
}


- (NSDictionary *)rateItemAtIndex:(int)index {
  return (rateContent != nil && [rateContent count] > 0 && index < [rateContent count]) ? [rateContent objectAtIndex:index] : nil;
}

- (int)rateCount {
  return (rateContent != nil) ? [rateContent count] : 0;
}

@end
